from rental.forms import BookForm, MovieForm, CDsForm
from rental.models import Book, Movie, CD
from django.shortcuts import redirect, render

categories = {
    'books': Book,
    'movies': Movie,
    'cds': CD
}

forms = {
    'books': BookForm,
    'movies': MovieForm,
    'cds': CDsForm
}

categories_pl = {
    'books': 'KSIĄŻKI',
    'movies': 'FILMY',
    'cds': "PŁYTY MUZYCZNE"
}

def products_list_view(request, category, *args, **kwargs):

    context = {
        'object_list': categories[category].objects.all(),
        'category': categories_pl[category]
    }

    return render(request, "products_list.html", context)


def product_create_view(request, category, *args, **kwargs):
    form = forms[category](request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect("../")

    context = {
        'form': form,
        'category': categories_pl[category]
    }

    return render(request, "products_create_form.html", context)


def product_update_view(request, category, id, *args, **kwargs):

    obj = categories[category].objects.get(id=id)
    form = forms[category](request.POST or None, instance=obj)

    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect("../")

    context = {
        'form': form
    }

    return render(request, "products_update_form.html", context)


def product_delete_view(request, category, id, *args, **kwargs):

    context = {
        'object_list': categories[category].objects.all(),
        'category': categories_pl[category]
    }

    if request.method == "POST":
        obj = categories[category].objects.get(id=id)
        obj.delete()
        return redirect("../../")
    
    return render(request, "products_list.html", context)