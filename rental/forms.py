from django import forms
from django.forms import fields
from django.forms.forms import Form
from django.forms.widgets import Textarea

from .models import Book, Movie, CD

class BookForm(forms.ModelForm):
    author = forms.CharField(
        required=True,
        widget = forms.TextInput(
            attrs={
                "placeholder": "Autor"
            }
        )
    )
    title = forms.CharField(
        required=True,
        widget=forms.Textarea( 
            attrs={
                "rows": 2,
                "columns": 2,
                "placeholder": "Tytuł"
            } 
        )
    )

    genre = forms.CharField(
        required=True,
        widget=forms.Textarea( 
            attrs={
                "rows": 2,
                "columns": 2,
                "placeholder": "Gatunek"
            } 
        )
    )
    
    isbn = forms.CharField(
        required=True,
        widget=forms.Textarea( 
            attrs={
                "rows": 2,
                "columns": 2,
                "placeholder": "Międzynarodowy nr książki"
            } 
        )
    )

    class Meta:
        model = Book
        fields = [
            'author',
            'title',
            'genre',
            'isbn'
        ]

class MovieForm(forms.ModelForm):
    director = forms.CharField(
        required=True,
        widget = forms.TextInput(
            attrs={
                "placeholder": "Director"
            }
        )
    )
    title = forms.CharField(
        required=True,
        widget=forms.Textarea( 
            attrs={
                "rows": 1,
                "columns": 2,
                 "placeholder": "Tytuł"
            } 
        )
    )

    genre = forms.CharField(
        required=True,
        widget=forms.Textarea( 
            attrs={
                "rows": 2,
                "columns": 2,
                "placeholder": "Gatunek"
            } 
        )
    )
    
    duration = forms.DecimalField(
        required=True,
        widget=forms.TextInput(
            attrs={
                "rows": 1,
                "columns": 2,
                "placeholder": "Czas trwania"
            } 
        )
    )

    class Meta:
        model = Movie
        fields = [
            'director',
            'title',
            'genre',
            'duration'
        ]

class CDsForm(forms.ModelForm):
    
    band = forms.CharField(
        required=True,
        widget = forms.TextInput(
            attrs={
                "placeholder": "Nazwa zespołu"
            }
        )
    )
    title = forms.CharField(
        required=True,
        widget=forms.Textarea( 
            attrs={
                "rows": 1,
                "columns": 2,
                "placeholder": 'Tytuł'
            } 
        )
    )

    genre = forms.CharField(
        required=True,
        widget=forms.Textarea( 
            attrs={
                "rows": 2,
                "columns": 2,
                "placeholder": "Gatunek"
            }
        )
    )
    
    listofsongs = forms.CharField(
        required=True,
        widget=forms.Textarea( 
            attrs={
                "rows": 4,
                "columns": 2,
                "placeholder": "Lista piosenek"
            } 
        )
    )

    duration = forms.DecimalField(
        required=True,
        widget=forms.TextInput(
            attrs={
                "rows": 2,
                "columns": 2,
                "placeholder": "Długość"
            } 
        )
    )

    class Meta:
        model = CD
        fields = [
            'band',
            'title',
            'genre',
            'listofsongs',
            'duration'
        ]