from django.urls import path

from .views import products_list_view, product_update_view, product_create_view, product_delete_view

app_name="rental"

urlpatterns = [
    path('<str:category>/', products_list_view, name='list'),
    path('<str:category>/create/', product_create_view, name='create'),
    path('<str:category>/<int:id>/', product_update_view, name='edit'),
    path('<str:category>/<int:id>/delete/', product_delete_view, name='delete'),
]