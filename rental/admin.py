from django.contrib import admin
from django.utils.html import format_html
from .models import Book, Movie, CD, Vote

class VoteInline(admin.TabularInline):
    model = Vote

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    search_fields = ['title']
    list_display = ['title', 'genre', 'show_url']
    list_filter = ['genre']
    inlines = [
        VoteInline
    ]

    def show_url(self, obj):
        if obj.url is not None:
            return format_html(f'<a href="{obj.url}" target="_blank"> {obj.url}</a>')
    
    show_url.short_description = 'URL'

@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'genre']
    list_filter = ['genre', 'title']

@admin.register(CD)
class CDAdmin(admin.ModelAdmin):
    list_display = ['band', 'title', 'genre']
    list_filter = ['genre', 'title']

@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ['id', 'book', 'reason']
    list_filter = ['book']

