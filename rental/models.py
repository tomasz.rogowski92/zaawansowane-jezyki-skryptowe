from django.db import models
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.forms import forms

class Book(models.Model):
    author = models.TextField(max_length=30);
    title = models.TextField(max_length=60);
    genre =  models.TextField(max_length=25);
    isbn = models.CharField(validators=[RegexValidator(regex=r'^\d{13}$', message="13 digits")], max_length=13, unique=True);
    id = models.AutoField(primary_key=True);
    url = models.URLField(null=True, blank=True);
    def get_url(self):
        return f"/products/books/{self.id}"

    def clean(self, *args, **kwargs):
            if (Book.objects.filter(title=self.title).exists() &
                Book.objects.filter(author=self.author).exists() &
                Book.objects.filter(genre=self.genre).exists() ):
                raise forms.ValidationError('Nie możesz dodać książki, której autor, tytuł i gatunek występują już w bazie!')

    def __str__(self):
         return self.title

class Movie(models.Model):
    director = models.TextField(max_length=30);
    title = models.TextField(max_length=60);
    genre =  models.TextField(max_length=60);
    duration = models.DecimalField(max_digits=5, decimal_places=2);
    id = models.AutoField(primary_key=True);
    def get_url(self):
        return f"/products/movies/{self.id}"
    
    def clean(self, *args, **kwargs):
        object = Movie.objects

        if (object.filter(director=self.director) and 
            object.filter(title=self.title) and 
            object.filter(duration=self.duration) ):
            raise forms.ValidationError('Czas trwania musi się różnić!')



class CD(models.Model):
    band = models.TextField(max_length=50);
    title = models.TextField(max_length=80);
    genre =  models.TextField(max_length=80);
    listofsongs = models.TextField(max_length=120);
    duration = models.DecimalField(max_digits=5, decimal_places=2);
    id = models.AutoField(primary_key=True);
    def get_url(self):
        return f"/products/cds/{self.id}"

    def clean(self, *args, **kwargs):

        object = CD.objects

        if (object.filter(band=self.band) and
            object.filter(listofsongs=self.listofsongs) ):
            raise forms.ValidationError('Dwie płyty tego samego zespołu, nie mogą mieć tych samych list utworów!')

        grupedBands = {}
        allcds = object.all()

        #zliczamy wszystkie bandy i ich gatunki
        for group in allcds:
            if group.band not in grupedBands:
                grupedBands[group.band] = []
                grupedBands[group.band].append(group.genre)
            elif group.genre not in grupedBands[group.band]:
                grupedBands[group.band].append(group.genre)

        if self.band not in grupedBands:
            grupedBands[self.band] = []
        
        if self.genre not in grupedBands[self.band] and len(grupedBands[self.band]) >= 2:
            raise forms.ValidationError('Płyty danego zespołu mogą mieć tylko dwa gatunki!')

class Vote(models.Model):
     book = models.ForeignKey(Book, on_delete=models.CASCADE)
     reason = models.TextField()

     def __str__(self):
        return f' ID {self.id}'